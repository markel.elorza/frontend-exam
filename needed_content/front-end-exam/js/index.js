var timeoutID;

$(document).ready(function() {
	$('article').click(function() {
		window.clearTimeout(timeoutID);
        $(this).toggleClass('selected');
		timeoutID = window.setTimeout(unselect, 3000);
    });

    $('#hide-selected-btn').click(() => {
        $('article.selected').hide();
		window.clearTimeout(timeoutID);
    });

    $('#reset-btn').click(() => {
        $('article').show().removeClass('selected');
    });

});

function unselect() {
	$('article.selected').removeClass('selected');
}