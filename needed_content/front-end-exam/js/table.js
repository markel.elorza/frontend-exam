window.onload = main;
var progress;
function main() {
	progress = document.getElementById('table-progress').value;
	var closeBtns = document.getElementsByClassName('delete-row');
	let addRow = document.getElementById('myClickBtn');
	
	addRow.onclick = easyWay;
	
	for ( let i = 0; i < closeBtns.length; i++) {
		closeBtns[i].onclick = function() {
				deleteRowOfTable(this);
			}
	}
}

function easyWay() {
	if (progress == 100) return;
    let table = document.getElementById('tabla');
    let count = document.getElementsByTagName('tr').length;
	let username = document.getElementById('username');
	let email = document.getElementById('email');
	let btn = document.createElement("BUTTON");
	btn.onclick = function() {
				deleteRowOfTable(this);
			};
	btn.textContent = "x";
    let newRow = table.insertRow(count);
    count++;
    let newCell1 = newRow.insertCell(0);
    newCell1.textContent = username.value;
    let newCell2 = newRow.insertCell(1);
    newCell2.textContent = email.value;
    let newCell3 = newRow.insertCell(2);
    newCell3.appendChild(btn);
	
	document.getElementById('table-progress').value += 10;
	progress +=10;
}

function deleteRowOfTable(el) {
	var i = el.parentNode.parentNode.rowIndex;
    document.getElementById("tabla").deleteRow(i);
	document.getElementById('table-progress').value -= 10;
	progress -=10;
}
